package com.thomas.study.tabhoststudy;

import android.support.v4.app.FragmentTabHost;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.thomas.study.tabhoststudy.Fragments.Fragment1;
import com.thomas.study.tabhoststudy.Fragments.Fragment2;
import com.thomas.study.tabhoststudy.Fragments.Fragment3;

public class Main2Activity extends AppCompatActivity {
        private FragmentTabHost mTabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mTabHost=(FragmentTabHost)findViewById(android.R.id.tabhost);
        mTabHost.setup(this,getSupportFragmentManager(),android.R.id.tabcontent);

        //mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("",getDrawable(R.drawable.tab2_1_selector)), Fragment1.class,null);//글대신 사진넣을시

                                //tabspec의 이름은 "1"이고 (tag).화면에 보여질글 "11", 나타낼 fragment ,전달할 bundle값
        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("11"), Fragment1.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator("22"), Fragment2.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator("33"), Fragment3.class,null);
        mTabHost.getTabWidget().getChildAt(0).setBackground(null);//tabhost메뉴 아래의 line (tabstrip)을 제거
        mTabHost.getTabWidget().getChildAt(1).setBackground(null);
        mTabHost.getTabWidget().getChildAt(2).setBackground(null);

    }
}

package com.thomas.study.tabhoststudy.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTabHost;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thomas.study.tabhoststudy.Fragments2.Fragment2_1;
import com.thomas.study.tabhoststudy.Fragments2.Fragment2_2;
import com.thomas.study.tabhoststudy.Fragments2.Fragment2_3;
import com.thomas.study.tabhoststudy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment3 extends Fragment {
    private FragmentTabHost mTabHost;

    public Fragment3() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_fragment3, container, false);

        mTabHost=(FragmentTabHost)v.findViewById(android.R.id.tabhost);
        mTabHost.setup(getContext(),getChildFragmentManager(),android.R.id.tabcontent);

        mTabHost.addTab(mTabHost.newTabSpec("1").setIndicator("",getResources().getDrawable(R.drawable.tab2_1_selector)), Fragment2_1.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("2").setIndicator("",getResources().getDrawable(R.drawable.tab2_2_selector)), Fragment2_2.class,null);
        mTabHost.addTab(mTabHost.newTabSpec("3").setIndicator("",getResources().getDrawable(R.drawable.tab2_3_selector)), Fragment2_3.class,null);
        mTabHost.getTabWidget().getChildAt(0).setBackground(null);
        mTabHost.getTabWidget().getChildAt(1).setBackground(null);
        mTabHost.getTabWidget().getChildAt(2).setBackground(null);

        return v;
    }

}

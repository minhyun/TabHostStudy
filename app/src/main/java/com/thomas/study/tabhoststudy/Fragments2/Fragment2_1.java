package com.thomas.study.tabhoststudy.Fragments2;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thomas.study.tabhoststudy.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment2_1 extends Fragment {


    public Fragment2_1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment2_1, container, false);
    }

}
